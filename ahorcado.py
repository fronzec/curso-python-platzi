
# TODO decirle al usuario que letras ya ha usado
# TODO encapsular el comportamiendo de la funcion run en diferentes funciones

import random
# Juego de ahorcado para el curso de python
IMAGES = [
'''
    COPIAR EL ASCCI ART 1
''', 
'''
    COPIAR EL ASCCI ART 2
''', 
'''
    COPIAR EL ASCCI ART 3
''', 
'''
    COPIAR EL ASCCI ART 4
''',
'''
    COPIAR EL ASCCI ART 5
''',
'''
    COPIAR EL ASCCI ART 6
''',
'''
    COPIAR EL ASCCI ART 7
''',
'''
    COPIAR EL ASCCI ART 8
'''
]

WORDS = ['lavadora','secadora','sofa','licuadora','cama','computadora','celular'
        'silla','pantalla','espejo','puerta','teclado','zapato']

def random_word():
    index = random.randint(0, len(WORDS) - 1)
    return WORDS[index]

def show_display_board(hidden_word, tries):
    print(IMAGES[tries])
    print('')
    print(hidden_word)
    print('''---*---*---*---*---*---*---''')

def run():
    word =  random_word()
    hidden_word = ['-'] * len(word)
    tries = 0

    while True:
        show_display_board(hidden_word, tries)
        current_letter = str(raw_input('Ingresa una letra:'))
        letter_indexes = []

        for i in range(len(word)):
            if(word[i] == current_letter):
                letter_indexes.append(i)
                
        if len(letter_indexes) == 0:
            tries += 1
            if tries == 7:
                show_display_board(hidden_word, tries)
                print('')
                print('Perdiste jajajajaja!!! La palabra correcta era: {}'.format(word))
                break
        else:
            for index in letter_indexes:
                hidden_word[index] = current_letter
            letter_indexes = []

        # Manejo de excepciones en python
        try:
            hidden_word.index('-')
        except ValueError:
            print('')
            print('Ganaste Prro!!! La palabra es {}'.format(word))
            break
        

    pass

if __name__ == '__main__':
    print('B I E N V E N I D O S  A  A H O R C A D O S')
    run()
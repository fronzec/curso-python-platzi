# -*- coding: utf-8 -*-
# Ejemplo de clase creado en python
# En python no existen como tal lo publico y lo privado
# Los metodos de instancia reciben el objeto self(la propia instancia)
# el metodo __init__ es conocido como el constructor
# pass es un placeholder para decirle a python que algo eventualmente va a ir ahi
#  Las variables de instancia se agregan referenciando al objeto, por convencion a cada metodo recibe en el primer
# parametro una variable que por convencion se llama self 
# que hace referencia a nuestra instancia

class Lamp:
    _LAMPS = ['''
        LAMPARA PRENDIDA
    ''',
    '''
        LAMPARA APAGADA
    '''
    ]

    def __init__(self):
        self._is_turned_on = False

    def turn_on(self):
        self._is_turned_on = True
        self._display_image()

    def turn_off(self):
        self._is_turned_on = False
        self._display_image()

    def _display_image(self):
        if self._is_turned_on:
            print(self._LAMPS[0])
        else:
            print(self._LAMPS[1])

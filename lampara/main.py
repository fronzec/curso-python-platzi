# -*- coding:utf-8 -*-
# Un módulo permite agrupar funcionalidad y ocultar la complejidad
# Para requerir un modulo podemos usar import


from lamp import Lamp

def run():
    lampara1 = Lamp()

    while True:
        command = str(raw_input('''
            ¿Qué deseas hacer?
            [p] render
            [a] pagar
            [s] alir
        
        '''))
        if command == 'p':
            lampara1.turn_on()
        elif command == 'a':
            lampara1.turn_off()
        else:
            break


if __name__ == '__main__':
    run()
# -*- coding: utf-8 -*-

import math

#usando un poco de matematicas para descubrir numeros primos
def is_prime_sqrt(n):
	if(n<2):
		return False
	root=int(math.sqrt(n))
	for a in range(2,root+1):
		if n%a==0:
			return False
	return True

def isPrime(number):
    if number<2:
        return False
    elif number == 2:
        return True
    elif number > 2 and number % 2 == 0:
        return False
    else:
        for i in range(3, number):
            if number % i == 0:
                return False
    return True
            
def main():
    number =  int(raw_input("Escribe un numero para determinar si es primo:"))
    result =  isPrime(number)
    if result:
        print("Tu número es primo!!!")
    else:
        print("Tu número NO es primo!!!")

if __name__ == '__main__':
    main()
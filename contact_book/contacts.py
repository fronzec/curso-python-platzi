# -*- coding: utf-8 -*-
# Interfaz Directorio de contactos
# Programa de linea de comandos

import csv

class Contact:
    
    def __init__(self, name, phone, email):
        self.name = name
        self.phone = phone
        self.email = email


class ContactBook:

    # Usando un atributo de clase para el nombre del archivo de persistencia
    FILE_NAME = 'contacts.csv'

    def __init__(self):
        self._contacts = []

    def add(self, name, phone, email):
        contact =  Contact(name, phone, email)
        # print(' {} {} {}'.format(name, phone, email))
        self._contacts.append(contact)
        self._save()
    
    def show_all(self):
        for contact in self._contacts:
            self._print_contact(contact)

    def _print_contact(self, contact):
        print('###############################################')
        print('Name: {}'.format(contact.name))
        print('Phone: {}'.format(contact.phone))
        print('Email: {}'.format(contact.email))
        print('\n')

    def delete(self, name):
        for idx, contact in enumerate(self._contacts):
            if name.lower() == contact.name.lower():
                del self._contacts[idx]
                print('El contacto {} ha sido eliminado'.format(contact.name))
                self._save()
                break
        
    def search(self, name):
        for contact in self._contacts:
            if contact.name.lower() == name.lower():
                self._print_contact(contact)
                break
        else:
            self._not_found(name)

    def _not_found(self, name):
        print('###############')
        print('No hemos encontrado el contacto {}'.format(name))
        print('###############')

    def update(self, name):
        for contact in self._contacts:
            if contact.name.lower() == name.lower():
                contact.name = str(input('Ingresa el nuevo nombre:'))
                contact.email = str(input('Ingresa el nuevo email:'))
                contact.phone = str(input('Ingresa el nuevo telefono:'))
                print('\n El contacto {} ha sido actualizado'.format(name))
                self._save()
                break
        else:
            self._not_found(name)

    def _save(self):
        # csv Comma Separated Values
        with open(ContactBook.FILE_NAME,'w') as f:
            writer = csv.writer(f)
            writer.writerow(('name','phone','email'))
            for contact in self._contacts:
                writer.writerow((contact.name,contact.phone, contact.email))



def run():
    contact_book = ContactBook()

    with open(ContactBook.FILE_NAME,'r') as f:
        reader = csv.reader(f)
        for idx,row in enumerate(reader):
            if idx == 0:
                continue
            else:
                contact_book.add(row[0], row[1], row[2])


    while True:
        command = str(input('''
            ¿Qué deseas hacer?

            [a]ñadir contacto
            [ac]tualizar contacto
            [b]uscar contacto
            [e]liminar contacto
            [l]istar contactos
            [s]alir
        '''))

        # Commands
        # a  Add Contact
        # ac Update a Contact
        # b  Search contact
        # e  Delete Contact
        # l  List all contacts
        # s  Exit from program


        if command == 'a':
            name = str(input('Escribe el nombre del contacto:'))
            phone = str(input('Escribe el telefono del contacto:'))
            email = str(input('Escribe el email del contacto:'))
            contact_book.add(name, phone, email)
        elif command == 'ac':
            print('actualizar contacto')
            name = str(input('¿Que contacto deseas actualizar?'))
            contact_book.update(name)

        elif command == 'b':
            name = str(input('Escribe el nombre del contacto:'))
            contact_book.search(name)

        elif command == 'e':
            name = str(input('¿Que contacto deseas eliminar?:'))
            contact_book.delete(name)

        elif command == 'l':
            contact_book.show_all()

        elif command == 's':
            break
        else:
            print('Comando no encontrado.')


if __name__ == '__main__':
    run()
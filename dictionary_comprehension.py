# Dictionary comprehesion
# Permite agregar sintaxis sugar a nuestro codigo
# Para un list comprehesion usamos []
# Para un dictionary comprehesion usamos {}


if __name__ == '__main__':
    pares = []
    for i in range(1, 31):
        if i % 2 == 0:
            pares.append(i)

    print(pares)
    # Ejemplo de list comprehesion
    even = [num for num in range(1,31) if num % 2 == 0]
    print(even)

    cuadrados = {}
    for num in range(1,11):
        cuadrados[num] = num**2
    print(cuadrados)
    # Ejemplo de dictionary comprehesion
    cuadrados2 = {num: num**2 for num in range (1,11)}
    print(cuadrados2)
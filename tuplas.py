#
# Tuplas en python
# Son similares a las listas, se pueden acceder a sus elementos por medio de sus indices
# La gran difernecia con las listas es que las tuplas son inmutables
# Una forma de crear una tupla es con los caracteres ()

if __name__ == '__main__':
    mi_tupla = (1,2,3)
    # En ocasiones se puede ver la siguiente sintacxis
    # La coma es solo para indicar a python que estamos creando una tupla y no un entero
    mi_tupla_2 = (1,)

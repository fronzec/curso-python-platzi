# Las listas si son mutables, no como los strings
# Los strings son un tipo especial de listas
# No todos los operadores son validos, depende del tipo de dato que tengamos guardados en la lista
# Los slices tambien pueden ser utilizados sobre las listas
# Existen varios metodos que se pueden usar con las listas, como por ejemplo append, pop, push, sort
# Podemos extender las listas para modificar el comportamiendo de los operadores

def operadores_listas():
    milista = []
    milista.append(1)

    milista2 = []
    milista2.append(2)
    milista2.append(3)
    milista2.append(4)
    # Podemos sumar listas
    milista3 = milista + milista2
    print(milista3)

    # Una lista puede multiplicarse
    milista4 = 'a'
    milista5 = milista4 * 10
    print(milista5)

    # Borrando elementos de una lista usando el valor 
    utiles = ['lapiz', 'calculadora', 'cuaderno']
    del utiles[0]
    print(utiles)

    # Deconstruir y construir strings
    casa = 'casa'
    print(casa)
    print(type(casa))
    lista_casa =list(casa)
    print(lista_casa)
    print(type(lista_casa))

    # Reconstruccion de strings
    casa2 = ''.join(lista_casa)
    print(casa2)



def slices_en_listas():
    print('TODO hacer los slices sobre listas')

if __name__ == '__main__':
    operadores_listas()
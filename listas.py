# Listas en python
def listas():
    # Creamos una lista vacia
    amigos = list()
    # Podemos agregar un nuevo elemento a la lista con la funcion append()
    amigos.append('pedro')
    print(amigos)
    amigos.append('enrique')
    amigos.append('alberto')
    print(amigos)
    # Podemo acceder a cada elemento individual en la lista
    primer_amigo=amigos[0]
    print(primer_amigo)
    segundo_amigo=amigos[1]
    print(segundo_amigo)
    tercero_amigo=amigos[2]
    print(tercero_amigo)


def average_temps(temps):
    sum_of_temps = 0
    for temp in temps:
        sum_of_temps += float(temp)
    return sum_of_temps / len(temps)


# Lista de terperaturas
if __name__ =='__main__':
    temperaturas = [21, 24, 22, 20, 23, 24]

    average = average_temps(temperaturas)
    print('La temperatura promedio es:{}'.format(average))

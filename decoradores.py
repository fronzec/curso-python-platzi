# -*- coding: utf-8 -*-
# Un decorador es una funcion que recibe como parametro una funcion y que modifica esa funcion y la regresa como una 
# nueva funcion, modifica o agrega comportamiento a la funcion de entrada
# Un decorador inicia con el caracter @
# El decorador solo es azucar sintactica

# En este ejemplo creamos una funcion que esta protegida por una contraseña

def protected(function):
    def wrapper(password):
        if password == 'platzi':
            return function()
        else:
            print('La contraseña es incorrecta')

    return wrapper

# Uso de un decorador 
@protected
def protected_function():
    print('Tu contraseña es correcta')


if __name__ == '__main__':
    password = str(raw_input('Ingresa tu contraseña:'))

    #Uso de un decorador de manera manual
    #wrapper = protected(protected_function)
    # wrapper(password)

    protected_function(password)
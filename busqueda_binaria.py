# -*- coding:utf-8 -*-
# Busqueda Binaria
# La busqueda binaria es una algoritmo de busqueda el cual
# funciona sobre una lista ordenada de valores
# En cada ronda reducimos nuestro rango de busqueda a ala mitad
# En cada llamada evaluamos si el numero de la mitad en nuestro 
# segmento de busqueda coinside con el numero que buscamos
# en caso de que el numero no coincida, evaluamos si niestro valor
# es menor o mayor para decidor si buscamos en el subconjunto superior
# o inferior
#

def generate_int_list():
    # TODO crear un metodo para generar una lista de enteros con numeros aleatorios
    pass

def order_list():
    # TODO crear el contenido de la funcion para ordenar una lista
    pass

def binary_search(numbers, number_to_find, ini, end):
    if ini > end:
        return False
    mid = (ini + end ) / 2
    if numbers[mid] ==  number_to_find:
        return True
    elif numbers[mid] > number_to_find:
        return binary_search(numbers, number_to_find,  ini, mid-1)
    else:
        return binary_search(numbers, number_to_find, mid+1, end)


if __name__ == '__main__':
    # TODO reemplazar el mocc de la lista por llamadas a los metodos
    numbers = [1,2,3,4,6,7,9,10,11,12,13,14,15, 25, 27, 28, 29, 41, 56, 76, 88,89]
    number_to_find=int(raw_input("Ingresa un numero para buscar:"))
    result = binary_search(numbers, number_to_find, 0, len(numbers) - 1)
    if result is True:
        print('EL número si está en la lista')
    else:
        print('El número no está en la lista')
# Sets en python
# Teoria de conjuntos
# Un set no puede tener elementos repetidos

if __name__ == '__main__':
    s = set([1,2,3])
    t = set([3,4,5])
    print(s.union(t))
    print(s.intersection(t))
    print(s.difference(t))
    print(t.difference(s))
    print(1 in t)
    print(1 in s)
    print(1 not in s)
    print(1 not in t)
    # Otras formas de usar sets es con los operadores 
    print(s | t)
    print(s & t)
    print(s - t)
    print(t - s)
    print(s ^ t)
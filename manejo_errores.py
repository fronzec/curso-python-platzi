# -*- coding: utf-8 -*-


countries = {
    'mexico':122,
    'colombia':49,
    'argentina':43,
    'chile':18,
    'peru':31
}

while True:

    country = str(raw_input('Escribe el nombre del pais:')).lower()
    if country == 'exit':
        break
    try :
        print('La poblacion de {} son {} millones'.format(country, countries[country]))
    except KeyError:
        print('Aun no tenemos el dato de la población del pais que nos diste :(')
    else:
        # Esta seccion se ejecuta si el bloque catch se ejecuto correctamente
        print("Bloque else")
    finally:
        print("Esta seccion siempre se ejecuta")
        # Esta seccion siempre se ejecuta
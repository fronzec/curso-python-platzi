# -*- coding:utf-8 -*-
# Lectura de archivos con python
# f.readlines() es un metodo que devuelve todas las lineas de texto en forma de lista
#  

def run():
    counter = 0
    with open('archivo.txt') as f:
        #print(f.readlines())#
        for line in f:
            counter += line.count('is')    
    print("El texto 'is' se encuentra {} veces en el texto".format(counter))


if __name__ == '__main__':
    run()

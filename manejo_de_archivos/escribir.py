# -*- coding:utf-8 -*-
# La funcion with es un context manager
# Si el archivo no existe lo crea
# En este caso 'myfile' es el manejador del archivo
# Sin el manejador de contexto la operacion open debe ir acompañada de una funcion close dentr de un bloque finally

def run():

    with open('numeros.txt','w') as myfile:
        for i in range(10):
            myfile.write(str(i))

if __name__ == '__main__':
        run()
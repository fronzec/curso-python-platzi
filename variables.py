# -*- coding: utf-8 -*-
#La linea anterior es para darle utf-8 como coding al archivo

#raw_input() fue reemplazado por input() en la version 3 de python
name = str(raw_input('¿Cuál es tu nombre?'))
print('Hola, ' + name + '!')
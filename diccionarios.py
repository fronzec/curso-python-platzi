# Diccionarios en python
# Los diccionarios en python son una generalizacion de las listas
# Todos los elementos tienen una clave y un valor
# La llave no se puede repetir en el diccionarios
# Existen dos formas de crear un diccionario en python una con {} y otra con dict()
# Por defecto si iteramos un efecto nos devuelve el valor de las keys
# Para acceder a los valores usamos diccionario.values()
# Si queremos obtener tanto las llaves como valores podemos usar el metodo iteritems()

if __name__ == '__main__':
    calificaciones = {}
    calificaciones['algoritmos'] = 9
    calificaciones['matematicas'] = 10
    calificaciones['geografia'] = 8
    calificaciones['web'] = 7
    calificaciones['bases_de_datos'] = 10

    for key in calificaciones:
        print(key)

    for value in calificaciones.values():
        print(value)
    
    for key,value in calificaciones.iteritems():
        print('clave:{} valor:{}'.format(key, value))

    suma_de_calificaciones = 0
    for calificacion in calificaciones.values():
        suma_de_calificaciones += calificacion
    
    print("Suma de calificaciones:{}".format(suma_de_calificaciones))
    promedio = suma_de_calificaciones / len(calificaciones.values())
    print("Nuestro promedio es:{}".format(str(promedio)))
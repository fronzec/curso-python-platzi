# -*- coding: utf-8 -*-

def foreign_change_calculator(ammount):
    mex_to_col_rate = 145.97
    return mex_to_col_rate * ammount

def run():
    print('Calculadora de divisas')
    print('Convierte pesos mexicanos a pesos colombianos\n')
    ammount= float(raw_input("Ingresa la cantidad en pesos mexicanos:"))
    result = foreign_change_calculator(ammount)
    print('${} pesos mexicanos son ${} pesos colombianos').format(ammount,result)

if __name__ == '__main__':
    run()